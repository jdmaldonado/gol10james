﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Gol10James.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Gol10James.Services
{
    public class ClientService
    {
        //Candela
        public Stream GenerateExcelFileForCandelaClients(List<CandelaClient> records, CultureInfo culture)
        {

            var headers = new List<object[]>();
            headers.Add(GetCandelaSpreadSheetHeader());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Candela");

            var recordsDecoded = new List<object[]>();

            foreach (var record in records)
                recordsDecoded.Add(ConvertCandelaClientToObject(record, culture));


            ws.Cells["A1"].LoadFromArrays(headers);

            using (ExcelRange rng = ws.Cells["A1:G1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                rng.Style.Font.Color.SetColor(Color.White);
            }

            if (recordsDecoded.Count > 0)
            {
                ws.Cells["A2"].LoadFromArrays(recordsDecoded);
            }

            return new MemoryStream(pck.GetAsByteArray());
        }

        private object[] GetCandelaSpreadSheetHeader()
        {
            return new object[] {
                                    "IdCliente", 
                                    "Cedula",
                                    "Nombre Completo", 
                                    "Teléfono",
                                    "Correo",
                                    "Pasaporte ?",
                                    "Digitador"
            };
        }

        private object[] ConvertCandelaClientToObject(CandelaClient client, CultureInfo culture)
        {
            var _pasaporte = client.Pasaporte ? "Si" : "No";

            return new object[] { client.IdCliente.ToString(),
                                    client.Cedula,
                                    client.Nombre,
                                    client.Telefono,
                                    client.Correo,
                                    _pasaporte,
                                    client.Operator
            };
        }


        //Vibra
        public Stream GenerateExcelFileForVibraClients(List<VibraClient> records, CultureInfo culture)
        {

            var headers = new List<object[]>();
            headers.Add(GetVibraSpreadSheetHeader());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Vibra");

            var recordsDecoded = new List<object[]>();

            foreach (var record in records)
                recordsDecoded.Add(ConvertVibraClientToObject(record, culture));


            ws.Cells["A1"].LoadFromArrays(headers);

            using (ExcelRange rng = ws.Cells["A1:L1"])
            {
                rng.Style.Font.Bold = true;
                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));
                rng.Style.Font.Color.SetColor(Color.White);
            }

            if (recordsDecoded.Count > 0)
            {
                ws.Cells["A2"].LoadFromArrays(recordsDecoded);
            }

            return new MemoryStream(pck.GetAsByteArray());
        }

        private object[] GetVibraSpreadSheetHeader()
        {
            return new object[] {
                                    "IdCliente", 
                                    "Cedula",
                                    "Nombre Madre", 
                                    "Teléfono",
                                    "Correo",
                                    "Pasaporte Madre?",
                                    "Digitador",
                                    "Documento Hijo",
                                    "Nombre Hijo",
                                    "Edad",
                                    "Pasaporte Hijo"
            };
        }

        private object[] ConvertVibraClientToObject(VibraClient client, CultureInfo culture)
        {
            var _pasaporteMadre = client.PasaporteMadre ? "Si" : "No";
            var _pasaporteHijo  = client.PasaporteHijo ? "Si" : "No";

            return new object[] { client.IdCliente.ToString(),
                                    client.Cedula,
                                    client.NombreMadre,
                                    client.Telefono,
                                    client.Correo,
                                    _pasaporteMadre,
                                    client.Operator,
                                    client.DocumentoHijo,
                                    client.NombreHijo,
                                    client.Edad,
                                    _pasaporteHijo
            };
        }
    }
}