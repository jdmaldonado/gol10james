﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;


namespace Gol10James.DataBase.Update
{
    public class OperatorsUpdates
    {
        public static dynamic UpdateOperator(Operator _operator)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            var updateOperator = db.Operadores.UpdateByIdOperador(_operator);

            return updateOperator;
        }
    }
}