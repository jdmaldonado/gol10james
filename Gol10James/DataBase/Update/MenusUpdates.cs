﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;


namespace Gol10James.DataBase.Update
{
    public class MenusUpdates
    {
        public static dynamic QuitRoleToProfile(int idPerfil, int idRol)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            var deletedRol = db.PerfilesRoles.Delete(idPerfil: idPerfil, idRol: idRol);

            return deletedRol;
        }
    }
}