﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;


namespace Gol10James.DataBase.Update
{
    public class ClientsUpdates
    {
        public static int UpdateCandelaClient(CandelaClient _client)
        {
            var db = Database.OpenNamedConnection("Gol10James");
            int _editedClient = db.ClientesCandela.UpdateByIdCliente(_client);

            return _editedClient;
        }

        public static int UpdateVibraClient(VibraClient _client)
        {
            var db = Database.OpenNamedConnection("Gol10James");
            int _editedClient = db.ClientesVibra.UpdateByIdCliente(_client);

            return _editedClient;
        }
    }
}