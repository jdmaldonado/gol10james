﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;


namespace Gol10James.DataBase.Update
{
    public class ProfilesUpdates
    {
        public static int UpdateProfile(Profile _profile)
        {
            var db = Database.OpenNamedConnection("Gol10James");
            int _editedProfile = db.Perfiles.UpdateByIdPerfil(_profile);

            return _editedProfile;
        }
    }
}