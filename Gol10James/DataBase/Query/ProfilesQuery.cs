﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Query
{
    public class ProfilesQuery
    {
        public static List<Profile> GetProfiles()
        {
            var db = Database.OpenNamedConnection("Gol10James");

            List<Profile> profiles = db.Perfiles.All();

            return profiles;
        }
    }
}