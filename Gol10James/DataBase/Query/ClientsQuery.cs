﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Query
{
    public class ClientsQuery
    {
        public static List<CandelaClient> GetCandelaClients()
        {
            var _db = Database.OpenNamedConnection("Gol10James");

            List<CandelaClient> _clients = _db.ClientesCandela.All();

            return _clients;
        }

        public static CandelaClient GetCandelaClientById(string clientId)
        {
            var _db = Database.OpenNamedConnection("Gol10James");

            CandelaClient _client = _db.ClientesCandela.All()
            .Where(_db.ClientesCandela.Cedula == clientId).FirstOrDefault();


            return _client;
        }

        public static List<VibraClient> GetVibraClients()
        {
            var _db = Database.OpenNamedConnection("Gol10James");

            List<VibraClient> _clients = _db.ClientesVibra.All();

            return _clients;
        }

        public static VibraClient GetVibraClientById(string clientId)
        {
            var _db = Database.OpenNamedConnection("Gol10James");

            VibraClient _client = _db.ClientesVibra.All()
            .Where(_db.ClientesVibra.Cedula == clientId).FirstOrDefault();


            return _client;
        }
    }
}