﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Query
{
    public class OperatorsQuery
    {
        public static List<Menu> ValidateOperator(long operatorCode)
        {
            var _db = Database.OpenNamedConnection("Gol10James");

            List<Menu> _menus = _db.Operadores
            .Select(
                _db.Operadores.IdOperador,
                _db.Operadores.Nombres,
                _db.Operadores.Codigo,
                _db.Roles.IdRol,
                _db.Roles.Rol,
                _db.Roles.Icono,
                _db.Roles.Href
            )
            .Join(_db.PerfilesRoles).On(_db.Operadores.IdPerfil == _db.PerfilesRoles.IdPerfil)
            .Join(_db.Roles).On(_db.PerfilesRoles.IdRol == _db.Roles.IdRol)
            .Where(_db.Operadores.Codigo == operatorCode)
            .OrderBy(_db.Roles.Rol);


            return _menus;
        }

        public static List<Operator> GetOperators()
        {
            var db = Database.OpenNamedConnection("Gol10James");

            List<Operator> operators = db.Operadores.All();

            return operators;
        }
    }
}