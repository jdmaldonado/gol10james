﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Query
{
    public class MenusQuery
    {
        public static List<Role> GetRolesByIdProfile(int idPerfil)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            List<Role> roles = db.sp_GetRolesByIdProfile(idPerfil);

            return roles;
        }
    }
}