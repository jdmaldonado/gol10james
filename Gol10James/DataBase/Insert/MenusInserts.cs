﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Insert
{
    public class MenusInserts
    {
        public static dynamic AddRoleToProfile(int idPerfil, int idRol)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            dynamic record = new SimpleRecord();
            record.IdPerfil = idPerfil;
            record.IdRol = idRol;

            var addedRol = db.PerfilesRoles.Insert(record);

            return addedRol;
        }
    }
}