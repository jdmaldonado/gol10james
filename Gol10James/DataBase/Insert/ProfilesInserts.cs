﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Insert
{
    public class ProfilesInserts
    {
        public static dynamic CreateProfile(Profile _profile)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            var _createdProfile = db.Perfiles.Insert(_profile);
            return _createdProfile;
        }
    }
}