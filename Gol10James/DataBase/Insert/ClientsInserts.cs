﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Insert
{
    public class ClientsInserts
    {
        public static dynamic CreateCandelaClient(CandelaClient _client)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            var _createdClient = db.ClientesCandela.Insert(_client);
            return _createdClient;
        }

        public static dynamic CreateVibraClient(VibraClient _client)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            var _createdClient = db.ClientesVibra.Insert(_client);
            return _createdClient;
        }
    }
}