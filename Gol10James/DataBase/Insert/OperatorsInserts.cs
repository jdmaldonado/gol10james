﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Simple.Data;
using Simple.Data.SqlServer;
using Gol10James.Models;

namespace Gol10James.DataBase.Insert
{
    public class OperatorsInserts
    {
        public static dynamic CreateOperator(Operator _operator)
        {
            var db = Database.OpenNamedConnection("Gol10James");

            var createdOperator = db.Operadores.Insert(_operator);


            return createdOperator;
        }
    }
}