﻿Gol10JamesApp.factory('Profiles', ['$resource', function ($resource) {

    return {
        profiles:
            $resource('Profiles', {},
                {
                    "GetProfiles": { method: "GET", params: {}, isArray: true }
                }
            ),
        updateProfile:
             $resource('Profiles/update', {},
                {
                    "putProfile": { method: "PUT", params: {}, isArray: false }
                }
            ),
        createProfile:
             $resource('Profiles/create', {},
                {
                    "postProfile": { method: "POST", params: {}, isArray: false }
                }
            )
    };

}]);