﻿Gol10JamesApp.factory('Notifications', ['toastr', function (toastr) {

    var toastrText;
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "2000"
    }

    return {
        success: {
            putSuccess: function () {
                toastrText = 'Registro actualizado con éxito';
                toastr.success(toastrText);
            },
            postSuccess: function () {
                toastrText = 'Registro creado con éxito';
                toastr.success(toastrText);
            },
            deleteSuccess: function () {
                toastrText = 'Registro eliminado con éxito';
                toastr.success(toastrText);
            }
        },
        errors: {
            putError: function () {
                toastrText = 'El servidor no pudo Procesar su solicitud';
                toastr.error(toastrText);
            },
            postError: function () {
                toastrText = 'Ha enviado una solicitud que el servidor no puede entender';
                toastr.error(toastrText);
            },
            serverError: function () {
                toastrText = 'Lo lamentamos, ha ocurrido un error en el servidor';
                toastr.error(toastrText);
            }
        }
    };

}]);