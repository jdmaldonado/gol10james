﻿Gol10JamesApp.factory('Operators', ['$resource', function ($resource) {

    return {
        validate:
            $resource('Operators/Validate/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "ValidateOperator": { method: "GET", params: { operatorCode: '@operatorCode' }, isArray: true }
                }
            ),
        operators:
           $resource('Operators', {},
               {
                   "GetOperators": { method: "GET", params: {}, isArray: true }
               }
           ),
        updateOperator:
             $resource('Operators/update', {},
                {
                    "putOperator": { method: "PUT", params: {}, isArray: false }
                }
            ),
        createOperator:
             $resource('Operators/create', {},
                {
                    "postOperator": { method: "POST", params: {}, isArray: false }
                }
            )
       
    };

}]);