﻿Gol10JamesApp.factory('Clients', ['$resource', function ($resource) {

    return {

        //Clientes Candela

        candela:
            $resource('Clients/Candela/:clientId', { clientId: '@clientId' },
                {
                    "GetClientById": { method: "GET", params: { clientId: '@clientId' }, isArray: false }
                }
            ),
        addCandela:
             $resource('Clients/Candela/add', {},
                {
                    "createClient": { method: "POST", params: {}, isArray: false }
                }
            ),
        updateCandela:
             $resource('Clients/Candela/update', {},
                {
                    "updateClient": { method: "PUT", params: {}, isArray: false }
                }
            ),
        
        //Clientes Vibra

        vibra:
            $resource('Clients/Vibra/:clientId', { clientId: '@clientId' },
                {
                    "GetClientById": { method: "GET", params: { clientId: '@clientId' }, isArray: false }
                }
            ),
        addVibra:
             $resource('Clients/Vibra/add', {},
                {
                    "createClient": { method: "POST", params: {}, isArray: false }
                }
            ),
        updateVibra:
             $resource('Clients/Vibra/update', {},
                {
                    "updateClient": { method: "PUT", params: {}, isArray: false }
                }
            ),
       
    };

}]);