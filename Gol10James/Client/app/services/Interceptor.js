﻿Gol10JamesApp.factory('Interceptor', ['toastr', 'Notifications', '$window', '$q', '$location', '$rootScope', function (toastr, Notifications, $window, $q, $location, $rootScope) {
    return {

        'response': function (response) {
            console.log(response);
            if (response.config.method == 'PUT' && response.status == 200) {
                Notifications.success.putSuccess();
            } else if (response.config.method == 'POST' && (response.status == 200 || response.status == 201)) {
                Notifications.success.postSuccess();
            } else if (response.config.method == 'DELETE' && response.status == 200) {
                Notifications.success.deleteSuccess();
            }
            return response;
        },
        'responseError': function (rejection) {
            console.log(rejection);
            if (rejection.status === 500) {
                Notifications.errors.serverError();
            } else if (rejection.config.method === 'PUT' && rejection.status === 400) {
                Notifications.errors.putError();
            } else if (rejection.config.method === 'POST' && rejection.status === 400) {
                Notifications.errors.postError();
            }

            return $q.reject(rejection);
        }
    };
}]);