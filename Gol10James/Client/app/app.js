var Gol10JamesApp = angular.module('Gol10JamesApp', ['ngResource', 'ui.router', 'ui.bootstrap', 'ngRoute'])

    .value("toastr", toastr)

    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider

        // Login
        .state('/', {
            url: '/',
            controller: 'login_ctrl',
            templateUrl: 'Client/app/templates/login/login.html'
        })

        // Home
        .state('home', {
            url: '/home/:operatorCode',
            controller: 'homeCtrl',
            templateUrl: 'Client/app/templates/home.html'
        })

        //Candela
        .state('candela', {
            url: '/candela/:operatorCode',
            controller: 'candela_ctrl',
            templateUrl: 'Client/app/templates/Clients/candela.html'
        })

        //Vibra
        .state('vibra', {
            url: '/vibra/:operatorCode',
            controller: 'vibra_ctrl',
            templateUrl: 'Client/app/templates/Clients/vibra.html'
        })

        //Vibra
        .state('list', {
            url: '/list/:operatorCode',
            controller: 'list_ctrl',
            templateUrl: 'Client/app/templates/Clients/list.html'
        })


        //----Management-----

        //Profiles
        .state('profiles', {
            url: '/profiles/:operatorCode',
            controller: 'profiles_ctrl',
            templateUrl: 'Client/app/templates/Management/profiles.html'
        })

        //Operadores
        .state('operators', {
            url: '/operators/:operatorCode',
            controller: 'operators_ctrl',
            templateUrl: 'Client/app/templates/Management/operators.html'
        })

        $httpProvider.interceptors.push('Interceptor');

        
            
    }])