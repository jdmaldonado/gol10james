Gol10JamesApp.controller('homeCtrl', ['$scope', '$rootScope', '$stateParams', 'Operators', function ($scope, $rootScope, $stateParams, Operators) {

    $scope.menus = [];
    $scope.settings = [];
    $scope.invalidUserMsg = false;
    $scope.showImages = false;
    $scope.showSettings = false;
    $scope.operatorCode = $stateParams.operatorCode;

    $scope.colsize = 12;

    //Separamos los roles entre Menus y configuraciones
    var EvaluateRollKind = function (data) {
        for (var i = 0 ; i < data.length ; i++) {
            if (data[i].idRol <= 3) {
                $scope.menus.push(data[i]);
            } else if (data[i].idRol >= 5) {
                $scope.showSettings = true;
                $scope.settings.push(data[i]);
            }
        }

        $scope.colsize = (12 / $scope.menus.length);
    }

    var validate = function (data) {
        if (data.length > 0) {
            $scope.showImages = true;
            EvaluateRollKind(data)
        } else {
            $scope.invalidUserMsg = true;
        }
    }

    Operators.validate.ValidateOperator({ operatorCode: $scope.operatorCode }).$promise.then(function (data) {
        validate(data);
    });

    

}])