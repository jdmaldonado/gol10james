﻿Gol10JamesApp.controller('list_ctrl', ['$scope', '$stateParams', 'Export', function ($scope, $stateParams, Export) {

    $scope.operatorCode = $stateParams.operatorCode;

    Export.downloadIt(null, "Clients/Historical/Candela");
    Export.downloadIt(null, "Clients/Historical/Vibra");

}])