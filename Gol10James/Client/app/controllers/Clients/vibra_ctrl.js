﻿Gol10JamesApp.controller('vibra_ctrl', ['$scope', '$stateParams', 'Clients', function ($scope, $stateParams, Clients) {


    $scope.operatorCode = $stateParams.operatorCode;

    $scope.filterContainer = true;
    $scope.formContainer = false;
    $scope.cliente = {};

    $scope.evaluateClient = function (client) {
        if (client.idCliente != null) {
            $scope.cliente = client;
        } else {
            $scope.cliente.cedula = $scope.cedula;
        }
    }


    $scope.Validate = function (cedula) {
        $scope.filterContainer = false;
        $scope.formContainer = true;

        Clients.vibra.GetClientById({ clientId: $scope.cedula }).$promise.then(function (client) {
            $scope.evaluateClient(client);
        });
    }

    $scope.Save = function () {
        $scope.cliente.operator = $scope.operatorCode;

        if ($scope.cliente.idCliente == null) {
            Clients.addVibra.createClient({}, $scope.cliente).$promise.then(function (data) {
            });
        }
        else {
            Clients.updateVibra.updateClient({}, $scope.cliente).$promise.then(function (data) {
            });
        }

        $scope.Clean();
    }

    $scope.Clean = function () {
        $scope.cliente = {};
        $scope.cedula = "";
        $scope.filterContainer = true;
        $scope.formContainer = false;
    }

}])