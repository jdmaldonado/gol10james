﻿Gol10JamesApp.controller('profiles_ctrl', ['$scope', '$rootScope', '$stateParams', 'Profiles', 'Menus', function ($scope, $rootScope, $stateParams, Profiles, Menus) {
    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $stateParams.operatorCode;

    $scope.view = {
        editInfo: false,
        form: true
    };

    $scope.formProfile = {}; /* Formulario Básico */
    $scope.perfiles = {}; /* Select de Filtro */

    /* Trae TODOS los clientes */
    var getProfiles = function () {
        $scope.perfiles = Profiles.profiles.GetProfiles({});
    }

    getProfiles();

    /* Muestra el Formulario de edición y oculta el formulario Principal*/
    $scope.loadEditForm = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.profile = undefined;
    }

    /* Limpia Los formularios */
    $scope.Clean = function () {
        $scope.view.editInfo = false;
        $scope.view.form = true;
        $scope.profile = undefined;
        $scope.formProfile = {};
        getProfiles();
    }

    /* Actualiza el formulario principal con la información del cliente seleccionado */
    $scope.Update = function (profileName) {

        /* Primero Esconda el formulario de edición y muestre el básico */
        $scope.view.editInfo = false;
        $scope.view.form = true;

        /*como solo recibimos la cédula hay que recorrer el listado de clientes y traer el que coincida, con esto llenamos el formulario base*/
        $scope.perfiles.map(function (objeto) {
            if (objeto.perfil == profileName) {
                $scope.formProfile = objeto;
            }
        })

    }

    /* Traiga los Roles y diga si están asociados actualmente o no al perfil */
    $scope.ManageRoles = function (profileName) {

        var obj_profile = {};
        $scope.perfiles.map(function (objeto) {
            if (objeto.perfil == profileName) {
                obj_profile = objeto;
            }
        })

        $scope.roles = Menus.roles.GetRolesByIdPerfil({ idPerfil: obj_profile.idPerfil });
    }

    /* Agregue o Elimine Roles a el perfil*/
    $scope.AddOrDeleteRol = function (obj_rol) {

        if (obj_rol.activo == false) {

            Menus.deleteRol.DeleteRolToPerfil({ idPerfil: obj_rol.idPerfil, idRol: obj_rol.idRol }).$promise.then(function () {
                $scope.ManageRoles(obj_rol.perfil);
            });
        }
        else {
            Menus.addRol.AddRolToPerfil({ idPerfil: obj_rol.idPerfil, idRol: obj_rol.idRol }).$promise.then(function () {
                $scope.ManageRoles(obj_rol.perfil);
            });
        }

    }

    /*Guarda o edita el Perfil*/
    $scope.Save = function () {

        /* Si Existe un idPerfil Quiere decir que se va a EDITAR, 
        de lo contrario voy a CREAR uno nuevo*/

        if ($scope.formProfile.idPerfil === undefined) {
            Profiles.createProfile.postProfile({}, $scope.formProfile).$promise.then(function () {
                $scope.Clean();
            });
        }
        else {
            Profiles.updateProfile.putProfile({}, $scope.formProfile).$promise.then(function () {
                $scope.Clean();
            });
        }
    }
}]);