﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Gol10James.DataBase.Query;
using Gol10James.DataBase.Insert;
using Gol10James.DataBase.Update;
using Gol10James.Models;
using Gol10James.Services;
using System.Globalization;
using Nancy.Responses;

namespace Gol10James.Modules
{
    public class ClientsModule : NancyModule
    {
        public ClientsModule(ClientService service)
            : base("Clients")
        {
            Get["/Candela/{clientId}"] = parameters =>
            {
                var _clientId = parameters.clientId;
                CandelaClient _client = ClientsQuery.GetCandelaClientById(_clientId);

                return _client;
            };

            Post["/Candela/add"] = parameters =>
            {
                var _client = this.Bind<CandelaClient>();

                var result = ClientsInserts.CreateCandelaClient(_client);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Candela/update"] = parameters =>
            {
                var _client = this.Bind<CandelaClient>();

                var result = ClientsUpdates.UpdateCandelaClient(_client);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };



            Get["/Vibra/{clientId}"] = parameters =>
            {
                var _clientId = parameters.clientId;
                VibraClient _client = ClientsQuery.GetVibraClientById(_clientId);

                return _client;
            };

            Post["/Vibra/add"] = parameters =>
            {
                var _client = this.Bind<VibraClient>();

                var result = ClientsInserts.CreateVibraClient(_client);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/Vibra/update"] = parameters =>
            {
                var _client = this.Bind<VibraClient>();

                var result = ClientsUpdates.UpdateVibraClient(_client);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Get["/Historical/Candela"] = parameters =>
            {
                List<CandelaClient> clients = ClientsQuery.GetCandelaClients();

                var culture = new CultureInfo("es-CO");

                var file = service.GenerateExcelFileForCandelaClients(clients, culture);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return response.AsAttachment("CandelaClients.xlsx");

            };

            Get["/Historical/Vibra"] = parameters =>
            {
                List<VibraClient> clients = ClientsQuery.GetVibraClients();

                var culture = new CultureInfo("es-CO");

                var file = service.GenerateExcelFileForVibraClients(clients, culture);
                var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return response.AsAttachment("VibraClients.xlsx");

            };

        }
    }
}