﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Gol10James.DataBase.Query;
using Gol10James.DataBase.Insert;
using Gol10James.DataBase.Update;

namespace Gol10James.Modules
{
    public class MenusModule : NancyModule
    {
        public MenusModule()
            : base("Menus")
        {
            
            Get["/Roles/{IdPerfil}"] = parameters =>
            {
                var idProfile = (int)parameters.IdPerfil;
                var roles = MenusQuery.GetRolesByIdProfile(idProfile);

                return roles;
            };

            Post["/Roles/{IdPerfil}/{IdRol}"] = parameters =>
            {
                var idPerfil = (int)parameters.IdPerfil;
                var idRol = (int)parameters.IdRol;
                var roles = MenusInserts.AddRoleToProfile(idPerfil, idRol);

                return roles;
            };

            Delete["/Roles/{IdPerfil}/{IdRol}"] = parameters =>
            {
                var idPerfil = (int)parameters.IdPerfil;
                var idRol = (int)parameters.IdRol;
                var roles = MenusUpdates.QuitRoleToProfile(idPerfil, idRol);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}