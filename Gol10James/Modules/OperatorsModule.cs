﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Gol10James.DataBase.Query;
using Gol10James.DataBase.Update;
using Gol10James.DataBase.Insert;
using Gol10James.Models;

namespace Gol10James.Modules
{
    public class OperatorsModule : NancyModule
    {
        public OperatorsModule()
            : base("Operators")
        {
            Get["/Validate/{operatorCode}"] = parameters =>
            {
                var _operatorCode = (long)parameters.operatorCode;
                var _menus = OperatorsQuery.ValidateOperator(_operatorCode);

                return _menus;
            };

            Get["/"] = parameters =>
            {
                var _operators = OperatorsQuery.GetOperators();

                return _operators;
            };

            Put["/update"] = parameters =>
            {
                var _operator = this.Bind<Operator>();

                var editedOperator = OperatorsUpdates.UpdateOperator(_operator);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/create"] = parameters =>
            {
                var _operator = this.Bind<Operator>();

                Operator createdOperator = OperatorsInserts.CreateOperator(_operator);

                return createdOperator;
            };
        
        }
    }
}