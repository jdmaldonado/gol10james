﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using Gol10James.DataBase.Query;
using Gol10James.DataBase.Update;
using Gol10James.DataBase.Insert;
using Gol10James.Models;

namespace ClientManagement.Modules
{
    public class ProfilesModule : NancyModule
    {
        public ProfilesModule()
            : base("Profiles")
        {
            Get["/"] = parameters =>
            {
                var profiles = ProfilesQuery.GetProfiles();

                return profiles;
            };

            Put["/update"] = parameters =>
            {
                var profile = this.Bind<Profile>();

                var editedProfile = ProfilesUpdates.UpdateProfile(profile);

                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Post["/create"] = parameters =>
            {
                var perfil = this.Bind<Profile>();

                Profile createdProfile = ProfilesInserts.CreateProfile(perfil);

                return createdProfile;
            };
        }
    }
}