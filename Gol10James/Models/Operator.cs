﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gol10James.Models
{
    public class Operator
    {
        public int IdOperador { get; set; }
        public long Codigo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public int IdPerfil { get; set; }
    }
}