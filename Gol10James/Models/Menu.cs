﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gol10James.Models
{
    public class Menu
    {
        public int IdOperador { get; set; }
        public long Codigo { get; set; }
        public string Nombres { get; set; }
        public int IdRol { get; set; }
        public string Rol { get; set; }
        public string Icono { get; set; }
        public string href { get; set; }
    }
}