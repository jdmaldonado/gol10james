﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gol10James.Models
{
    public class Role
    {
        public int IdPerfil { get; set; }
        public string Perfil { get; set; }
        public int IdRol { get; set; }
        public string rol { get; set; }
        public Boolean Activo { get; set; }
    }
}