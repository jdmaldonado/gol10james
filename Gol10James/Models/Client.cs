﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gol10James.Models
{
    public class CandelaClient
    {
        public int? IdCliente { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public Boolean Pasaporte{ get; set; }
        public long Operator { get; set; }
        
    }

    public class VibraClient
    {
        public int? IdCliente { get; set; }
        /*Mamá*/
        public string Cedula { get; set; }
        public string NombreMadre { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public Boolean PasaporteMadre { get; set; }
        /*Hijo*/
        public string DocumentoHijo { get; set; }
        public string NombreHijo { get; set; }
        public int Edad { get; set; }
        public Boolean PasaporteHijo { get; set; }
        public long Operator { get; set; }

    }
}